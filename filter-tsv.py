#!/usr/bin/python3
#coding=utf-8








import sys
import argparse
try:
    from pywraps2 import S2Cell, S2CellId, S2CellUnion, S2LatLng, S2LatLngRect, S2Loop, S2RegionCoverer
except ModuleNotFoundError:
    pass
try:
    from geopy import distance
except ModuleNotFoundError:
    pass




#constants
GYM_S2_CELL_LEVEL = 14




def latitude_degree(l):
    try:
        l = float(l)
    except ValueError:
        raise argparse.ArgumentTypeError("“{}” is not a floating-point number.".format(l))
    
    if l < -90  or  l > 90:
        raise argparse.ArgumentTypeError("“{}” is not within the range [-90.0; 90.0].".format(l))
    
    return l




def longitude_degree(l):
    try:
        l = float(l)
    except ValueError:
        raise argparse.ArgumentTypeError("“{}” is not a floating-point number.".format(l))
    
    if l < -180  or  l > 180:
        raise argparse.ArgumentTypeError("“{}” is not within the range [-180.0; 180.0].".format(l))
    
    return l




def point_degree(p):
    return ( latitude_degree(p[0]), longitude_degree(p[1]) )




#check which optional modules have been loaded
if "pywraps2" in sys.modules:
    s2geometry_loaded = True
else:
    s2geometry_loaded = False
if "geopy" in sys.modules:
    geopy_loaded = True
else:
    geopy_loaded = False


#parse command arguments
parser = argparse.ArgumentParser()
parser.add_argument("--within-rectangle", nargs=4, default=[], action="append", type=float, help="filter records contained within the spherical rectangle spanned by LATITUDE-MAX,LONGITUDE-MIN and LATITUDE-MIN,LONGITUDE-MAX", metavar=("LATITUDE-MAX", "LONGITUDE-MIN", "LATITUDE-MIN", "LONGITUDE-MAX"))
if s2geometry_loaded:
    parser.add_argument("--within-simple-polygon", default=[], action="append", type=str, help="filter records contained within the simple polygon whose vertices are read from FILE", metavar="FILE")
    parser.add_argument("--within-cells", default=[], action="append", type=str, help="filter records contained within the cells (at any level) whose Cell ID Hex-Tokens are read from FILE", metavar="FILE")
    parser.add_argument("--within-gym-cells-covered-rectangle", nargs=4, default=[], action="append", type=float, help="filter records contained within the Gym cells covering the spherical rectangle spanned by LATITUDE-MAX,LONGITUDE-MIN and LATITUDE-MIN,LONGITUDE-MAX", metavar=("LATITUDE-MAX", "LONGITUDE-MIN", "LATITUDE-MIN", "LONGITUDE-MAX"))
    parser.add_argument("--within-gym-cells-covered-simple-polygon", default=[], action="append", type=str, help="filter records contained within the Gym cells covering the simple polygon whose vertices are read from FILE", metavar="FILE")
    parser.add_argument("--within-gym-cells-covered-cells", default=[], action="append", type=str, help="filter records contained within the Gym cells covering the cells (at any level) whose Cell ID Hex-Tokens are read from FILE", metavar="FILE")
if geopy_loaded:
    parser.add_argument("--within-circle", nargs=3, default=[], action="append", type=float, help="filter records contained within the circle with centre LATITUDE,LONGITUDE and radius RADIUS", metavar=("LATITUDE", "LONGITUDE", "RADIUS"))
args = parser.parse_args()

if not s2geometry_loaded:
    args.within_simple_polygon = []
    args.within_cells = []
    args.within_gym_cells_covered_rectangle = []
    args.within_gym_cells_covered_simple_polygon = []
    args.within_gym_cells_covered_cells = []
if not geopy_loaded:
    args.within_circle = []

#check whether valid latitudes and longitudes have been specified
for rectangle  in  args.within_rectangle + args.within_gym_cells_covered_rectangle:
    latitude_degree(rectangle[0])
    longitude_degree(rectangle[1])
    latitude_degree(rectangle[2])
    longitude_degree(rectangle[3])
for cricle in args.within_circle:
    latitude_degree(cricle[0])
    longitude_degree(cricle[1])


#generate simple polygons
#(Note: These are the ones not to be covered with gym cells.)
all_simple_polygons = set()
for simple_polygon_file_name in args.within_simple_polygon:
    simple_polygon_file = open(simple_polygon_file_name, "r")
    simple_polygon_file_vertices = [ S2LatLng.FromDegrees(  *point_degree( vertex.rstrip().split(" ", 1) )  ).ToPoint()  for  vertex in simple_polygon_file ]
    simple_polygon_file.close()
    
    simple_polygon = S2Loop(simple_polygon_file_vertices)
    #normalise the loop (if necessary), which is required because it’s vertices could have been given in clockwise orientation (which would typically lead to an extremely large simple polygon)
    simple_polygon.Normalize()
    
    all_simple_polygons.add(simple_polygon)


#generate overall cells region
#Note: Since all_cellIDs is a list it may contain duplicates, moreover, its cells may be at any level and non-normalised. However, normalisation (which includes sorting out duplicates) takes place later, when the S2CellUnion cells_overall_region is created from the list.
all_cellIDs = list()
if args.within_gym_cells_covered_rectangle or args.within_gym_cells_covered_simple_polygon or args.within_gym_cells_covered_cells:
    rc = S2RegionCoverer()
    rc.set_fixed_level(GYM_S2_CELL_LEVEL)

for cells_file_name in args.within_cells:
    cells_file = open(cells_file_name, "r")
    cells_file_cellIDs = [ S2CellId.FromToken( token.rstrip(), len(token.rstrip()) )  for  token in cells_file ]
    cells_file.close()
    
    all_cellIDs.extend(cells_file_cellIDs)

for rectangle in args.within_gym_cells_covered_rectangle:
    northwest_vertex = S2LatLng.FromDegrees(rectangle[0], rectangle[1])
    southeast_vertex = S2LatLng.FromDegrees(rectangle[2], rectangle[3])
    region_cellIDs = rc.GetCovering( S2LatLngRect.FromPointPair(northwest_vertex, southeast_vertex) )
    
    all_cellIDs.extend(region_cellIDs)

for simple_polygon_file_name in args.within_gym_cells_covered_simple_polygon:
    simple_polygon_file = open(simple_polygon_file_name, "r")
    simple_polygon_file_vertices = [ S2LatLng.FromDegrees(  *point_degree( vertex.rstrip().split(" ", 1) )  ).ToPoint()  for  vertex in simple_polygon_file ]
    simple_polygon_file.close()
    
    simple_polygon = S2Loop(simple_polygon_file_vertices)
    #normalise the loop (if necessary), which is required because it’s vertices could have been given in clockwise orientation (which would typically lead to an extremely large simple polygon)
    simple_polygon.Normalize()
    
    region_cellIDs = rc.GetCovering(simple_polygon)
    
    all_cellIDs.extend(region_cellIDs)

for cells_file_name in args.within_gym_cells_covered_cells:
    cells_file = open(cells_file_name, "r")
    cells_file_cellIDs = [ S2CellId.FromToken( token.rstrip(), len(token.rstrip()) )  for  token in cells_file ]
    cells_file.close()
    region_cellIDs = rc.GetCovering( S2CellUnion(cells_file_cellIDs) )
    
    all_cellIDs.extend(region_cellIDs)

cells_overall_region = S2CellUnion(all_cellIDs)


#filter and print TSV records
for record in sys.stdin:
    record = record.rstrip().split("\t", 2)
    latitude = float(record[0])
    longitude = float(record[1])
    name = record[2]
    
    cell = S2Cell(S2LatLng.FromDegrees(latitude, longitude))
    
    record_contained_within_region = False
    
    for rectangle in args.within_rectangle:
        if latitude >= rectangle[2] and latitude <= rectangle[0]  and  longitude >= rectangle[1] and longitude <= rectangle[3]:
            print( "{}\t{}\t{}".format(latitude, longitude, name) )
            record_contained_within_region = True
            break
    if record_contained_within_region:
        continue
    
    for simple_polygon in all_simple_polygons:
        if simple_polygon.Contains(cell):
            print( "{}\t{}\t{}".format(latitude, longitude, name) )
            record_contained_within_region = True
            break
    if record_contained_within_region:
        continue
    
    if cells_overall_region.Contains(cell):
        print( "{}\t{}\t{}".format(latitude, longitude, name) )
        continue
    
    for cricle in args.within_circle:
        if distance.distance( (latitude,longitude), (cricle[0],cricle[1]) ).m  <=  cricle[2]:
            print( "{}\t{}\t{}".format(latitude, longitude, name) )
            record_contained_within_region = True
            break
    #if record_contained_within_region:
    #    continue
















#Copyright © 2020, Christoph Anton Mitterer <mail@christoph.anton.mitterer.name>.
#All rights reserved.
#
#
#This program is free software: you can redistribute it and/or modify it under
#the terms of the GNU General Public License as published by the Free Software
#Foundation, either version 3 of the License, or (at your option) any later
#version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#PARTICULAR PURPOSE.
#See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License along with
#this program. If not, see <http://www.gnu.org/licenses/>.
