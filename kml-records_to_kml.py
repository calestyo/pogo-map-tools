#!/usr/bin/python3
#coding=utf-8








import sys
import os
import argparse




def printHeader(output_file, extra_header):
    output_file.write('<?xml version="1.0" encoding="UTF-8"?>\n' +
                      '<kml xmlns="http://www.opengis.net/kml/2.2">\n' +
                      "\t<Document>\n")
    if extra_header is not None:
        output_file.write(extra_header)




def printFooter(output_file, extra_footer):
    if extra_footer is not None:
        output_file.write(extra_footer)
    output_file.write("\t</Document>\n" +
                      "</kml>\n")




#parse command arguments
parser = argparse.ArgumentParser()
parser.add_argument("--output", type=os.path.splitext, help="write output to file_name", metavar="FILE")
parser.add_argument("--extra-header", type=str, help="file with an additional header to add", metavar="FILE")
parser.add_argument("--extra-footer", type=str, help="file with an additional footer to add", metavar="FILE")
parser.add_argument("--split-after", default=0, type=int, help="start new numbered output file after NUMBER records", metavar="NUMBER")
args = parser.parse_args()


if args.output:
    args.output = list(args.output)
    
    #consider only “.kml” and “.xml” as recognised extensions
    if args.output[1] not in [".kml", ".xml"]:
        args.output[0] += args.output[1]
        args.output[1] = ""


#read extra header and footer
if args.extra_header:
    extra_header_file = open(args.extra_header, "r")
    extra_header = extra_header_file.read()
    extra_header_file.close()
else:
    extra_header = None

if args.extra_footer:
    extra_footer_file = open(args.extra_footer, "r")
    extra_footer = extra_footer_file.read()
    extra_footer_file.close()
else:
    extra_footer = None


#set initial file for writing
file_number = 0
if args.output: #write to file
    output_file = open("".join(args.output), "x")
else: #write to standard output
    if args.split_after > 0:
        parser.error("Using “--split-after NUMBER” with NUMBER > 0 does not work with writing to standard output but requires the use of “--output FILE”.")
    output_file = sys.stdout


#print KML document(s)
printHeader(output_file, extra_header)

record_number = 0
for record in sys.stdin:
    record_number += 1
    
    #open next file, if necessary
    if args.split_after > 0  and  record_number > args.split_after:
        #handle the first split
        if file_number == 0:
            
            #add separator for the file number
            args.output[0] += "."
            
            #rename the inital file to contain the file number
            lock_file = open(str(file_number).join(args.output), "x")
            lock_file.close()
            
            os.replace(output_file.name, str(file_number).join(args.output))
        
        printFooter(output_file, extra_footer)
        output_file.close()
        
        file_number += 1
        record_number = 1
        
        output_file = open(str(file_number).join(args.output), "x")
        printHeader(output_file, extra_header)
    
    #print KML records
    output_file.write("\t\t" + record)

printFooter(output_file, extra_footer)

output_file.close()
















#Copyright © 2020, Christoph Anton Mitterer <mail@christoph.anton.mitterer.name>.
#All rights reserved.
#
#
#This program is free software: you can redistribute it and/or modify it under
#the terms of the GNU General Public License as published by the Free Software
#Foundation, either version 3 of the License, or (at your option) any later
#version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#PARTICULAR PURPOSE.
#See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License along with
#this program. If not, see <http://www.gnu.org/licenses/>.
