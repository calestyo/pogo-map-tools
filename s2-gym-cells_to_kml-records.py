#!/usr/bin/python3
#coding=utf-8








import warnings
import sys
import argparse
from xml.sax.saxutils import escape
from pywraps2 import S2Cell, S2CellId, S2CellUnion, S2LatLng, S2LatLngRect, S2Loop, S2RegionCoverer




#constants
GYM_S2_CELL_LEVEL = 14
DEFAULT_MAX_RECORDS_PER_REGION = 10000
DEFAULT_STYLEURL = "#s2cell_gym_default"




def latitude_degree(l):
    try:
        l = float(l)
    except ValueError:
        raise argparse.ArgumentTypeError("“{}” is not a floating-point number.".format(l))
    
    if l < -90  or  l > 90:
        raise argparse.ArgumentTypeError("“{}” is not within the range [-90.0; 90.0].".format(l))
    
    return l




def longitude_degree(l):
    try:
        l = float(l)
    except ValueError:
        raise argparse.ArgumentTypeError("“{}” is not a floating-point number.".format(l))
    
    if l < -180  or  l > 180:
        raise argparse.ArgumentTypeError("“{}” is not within the range [-180.0; 180.0].".format(l))
    
    return l




def point_degree(p):
    return ( latitude_degree(p[0]), longitude_degree(p[1]) )




def warn_max_records_per_region(l):
    if l == args.max_records_per_region:
        warnings.warn("Maximum number of records (cells) per region ({}) has been reached.".format(args.max_records_per_region))
    elif l > args.max_records_per_region:
        warnings.warn("Maximum number of records (cells) per region ({}) has been exceeded by {}).".format(args.max_records_per_region, l-args.max_records_per_region))




def get_number_of_missing_pokestops_for_next_gym(number_of_gyms, number_of_pokestops):
    number_of_occupants = number_of_gyms + number_of_pokestops
    
    if number_of_gyms == 0:
        if number_of_occupants < 2:
            return 2 - number_of_occupants
        else:
            return 0
    elif number_of_gyms == 1:
        if number_of_occupants < 6:
            return 6 - number_of_occupants
        else:
            return 0
    elif number_of_gyms == 2:
        if number_of_occupants < 20:
            return 20 - number_of_occupants
        else:
            return 0
    else: #number_of_gyms >= 3
        return -1




#parse command arguments
parser = argparse.ArgumentParser()
parser.add_argument("--cover-rectangle", nargs=4, default=[], action="append", type=float, help="cover the spherical rectangle spanned by LATITUDE-MAX,LONGITUDE-MIN and LATITUDE-MIN,LONGITUDE-MAX with cells", metavar=("LATITUDE-MAX", "LONGITUDE-MIN", "LATITUDE-MIN", "LONGITUDE-MAX"))
parser.add_argument("--fill-rectangle", nargs=4, default=[], action="append", type=float, help="fill (that is: cover the interior of) the spherical rectangle spanned by LATITUDE-MAX,LONGITUDE-MIN and LATITUDE-MIN,LONGITUDE-MAX with cells", metavar=("LATITUDE-MAX", "LONGITUDE-MIN", "LATITUDE-MIN", "LONGITUDE-MAX"))
parser.add_argument("--cover-simple-polygon", default=[], action="append", type=str, help="cover the simple polygon whose vertices are read from FILE with cells", metavar="FILE")
parser.add_argument("--fill-simple-polygon", default=[], action="append", type=str, help="fill (that is: cover the interior of) the simple polygon whose vertices are read from FILE with cells", metavar="FILE")
parser.add_argument("--cover-cells", default=[], action="append", type=str, help="cover the cells (at any level) whose Cell ID Hex-Tokens are read from FILE with cells", metavar="FILE")
parser.add_argument("--fill-cells", default=[], action="append", type=str, help="fill (that is: cover the interior of) the cells (at any level) whose Cell ID Hex-Tokens are read from FILE with cells", metavar="FILE")
parser.add_argument("--max-records-per-region", default=DEFAULT_MAX_RECORDS_PER_REGION, type=int, help="maximum number of records (cells) to be generated per region (ignored when covering)", metavar="NUMBER")
parser.add_argument("--gyms", type=str, help="TSV file with Gyms", metavar="FILE")
parser.add_argument("--pokestops", type=str, help="TSV file with PokéStops", metavar="FILE")
parser.add_argument("--no-descriptions", action="store_false", dest="descriptions", help="disable cell descriptions")
parser.add_argument("--stylise-cells-per-number-of-gyms", nargs=2, action="append", type=str, dest="style_configs", help="cells that contain NUMBER Gyms are stylised with STYLEURL", metavar=("(NUMBER|*)", "STYLEURL"))
parser.add_argument("--output-gyms-with-count-within-cell", type=str, help="generate TSV file with Gyms, each in addition with the count of Gyms within its own cell or “0” if it’s not contained in any of those generated", metavar="FILE")
parser.add_argument("--analyse", action="store_true", help="print additional information to standard error")
args = parser.parse_args()

#check whether valid latitudes and longitudes have been specified
for rectangle  in  args.cover_rectangle + args.fill_rectangle:
    latitude_degree(rectangle[0])
    longitude_degree(rectangle[1])
    latitude_degree(rectangle[2])
    longitude_degree(rectangle[3])

if args.style_configs:
    style_configs = dict(args.style_configs)
else:
    style_configs = {}


#build lists with cells as well as latitudes, longitudes and names for the records of the Gyms TSV file
gyms = []
if args.gyms:
    tsv_file = open(args.gyms, "r")
    for record in tsv_file:
        record = record.rstrip().split("\t", 2)
        latitude = float(record[0])
        longitude = float(record[1])
        name = record[2]
        
        gym_cell = S2Cell(S2LatLng.FromDegrees(latitude, longitude))
        gyms.append([gym_cell, latitude, longitude, name, 0]) #assume the gym wouldn’t be contained in any gym cell, by setting the count to “0”
    tsv_file.close()

#build lists with cells as well as names for the records of the PokéStops TSV file
pokestops = []
if args.pokestops:
    tsv_file = open(args.pokestops, "r")
    for record in tsv_file:
        record = record.rstrip().split("\t", 2)
        latitude = float(record[0])
        longitude = float(record[1])
        name = record[2]
        
        pokestop_cell = S2Cell(S2LatLng.FromDegrees(latitude, longitude))
        pokestops.append([pokestop_cell, name])
    tsv_file.close()


#cover/fill desired region(s) with GYM_S2_CELL_LEVEL-level cells
#Note: Since all_cellIDs is a set it cannot contain duplicates, moreover, its cells are all at the same level. Therefore, all_cellIDs is automatically normalised at the fixed level.
all_cellIDs = set()
rc = S2RegionCoverer()
rc.set_fixed_level(GYM_S2_CELL_LEVEL)
rc.set_max_cells(args.max_records_per_region)

for rectangle in args.cover_rectangle:
    northwest_vertex = S2LatLng.FromDegrees(rectangle[0], rectangle[1])
    southeast_vertex = S2LatLng.FromDegrees(rectangle[2], rectangle[3])
    region_cellIDs = rc.GetCovering( S2LatLngRect.FromPointPair(northwest_vertex, southeast_vertex) )
    warn_max_records_per_region(len(region_cellIDs))
    
    all_cellIDs.update(region_cellIDs)

for rectangle in args.fill_rectangle:
    northwest_vertex = S2LatLng.FromDegrees(rectangle[0], rectangle[1])
    southeast_vertex = S2LatLng.FromDegrees(rectangle[2], rectangle[3])
    region_cellIDs = rc.GetInteriorCovering( S2LatLngRect.FromPointPair(northwest_vertex, southeast_vertex) )
    warn_max_records_per_region(len(region_cellIDs))
    
    all_cellIDs.update(region_cellIDs)

for simple_polygon_file_name in args.cover_simple_polygon:
    simple_polygon_file = open(simple_polygon_file_name, "r")
    simple_polygon_file_vertices = [ S2LatLng.FromDegrees(  *point_degree( vertex.rstrip().split(" ", 1) )  ).ToPoint()  for  vertex in simple_polygon_file ]
    simple_polygon_file.close()
    
    simple_polygon = S2Loop(simple_polygon_file_vertices)
    #normalise the loop (if necessary), which is required because it’s vertices could have been given in clockwise orientation (which would typically lead to an extremely large simple polygon)
    simple_polygon.Normalize()
    
    region_cellIDs = rc.GetCovering(simple_polygon)
    warn_max_records_per_region(len(region_cellIDs))
    
    all_cellIDs.update(region_cellIDs)

for simple_polygon_file_name in args.fill_simple_polygon:
    simple_polygon_file = open(simple_polygon_file_name, "r")
    simple_polygon_file_vertices = [ S2LatLng.FromDegrees(  *point_degree( vertex.rstrip().split(" ", 1) )  ).ToPoint()  for  vertex in simple_polygon_file ]
    simple_polygon_file.close()
    
    simple_polygon = S2Loop(simple_polygon_file_vertices)
    #normalise the loop (if necessary), which is required because it’s vertices could have been given in clockwise orientation (which would typically lead to an extremely large simple polygon)
    simple_polygon.Normalize()
    
    region_cellIDs = rc.GetInteriorCovering(simple_polygon)
    warn_max_records_per_region(len(region_cellIDs))
    
    all_cellIDs.update(region_cellIDs)

for cells_file_name in args.cover_cells:
    cells_file = open(cells_file_name, "r")
    cells_file_cellIDs = [ S2CellId.FromToken( token.rstrip(), len(token.rstrip()) )  for  token in cells_file ]
    cells_file.close()
    region_cellIDs = rc.GetCovering( S2CellUnion(cells_file_cellIDs) )
    warn_max_records_per_region(len(region_cellIDs))
    
    all_cellIDs.update(region_cellIDs)

for cells_file_name in args.fill_cells:
    cells_file = open(cells_file_name, "r")
    cells_file_cellIDs = [ S2CellId.FromToken( token.rstrip(), len(token.rstrip()) )  for  token in cells_file ]
    cells_file.close()
    region_cellIDs = rc.GetInteriorCovering( S2CellUnion(cells_file_cellIDs) )
    warn_max_records_per_region(len(region_cellIDs))
    
    all_cellIDs.update(region_cellIDs)


#print KML records
for cellID in all_cellIDs:
    cell = S2Cell(cellID)
    vertices = [ S2LatLng(cell.GetVertex(i))  for i in range(4) ]
    coordinates = [ "{},{},0".format( v.lng().degrees(), v.lat().degrees() )  for v in vertices ]
    
    #determine the gyms within the cell
    gyms_within_cell = []
    for g in gyms:
        if cell.Contains(g[0]):
            gyms_within_cell.append(g)
    number_of_gyms = len(gyms_within_cell)
    
    #for each gym (found) within the cell, set the count of gyms within its own (that is: “this”) cell
    for g in gyms_within_cell:
        g[4] = number_of_gyms
    
    #determine the pokestops within the cell
    pokestops_within_cell = []
    for p in pokestops:
        if cell.Contains(p[0]):
            pokestops_within_cell.append(p)
    number_of_pokestops = len(pokestops_within_cell)
    
    #determine the number of missing pokestops for the next gym
    if args.gyms and args.pokestops:
        number_of_missing_pokestops_for_next_gym = get_number_of_missing_pokestops_for_next_gym(number_of_gyms, number_of_pokestops)
        if number_of_missing_pokestops_for_next_gym > 0:
            next_gym_long = "expectedly missing for next Gym: {}".format(number_of_missing_pokestops_for_next_gym)
            next_gym_short = str(number_of_missing_pokestops_for_next_gym)
        elif number_of_missing_pokestops_for_next_gym == 0:
            next_gym_long = "unknown / next Gym already expected"
            next_gym_short = "❓"
        else:
            next_gym_long = "expectedly no further Gyms possible"
            next_gym_short = "⛔"
    
    #construct name
    name = "<name>"
    if args.gyms:
        name += "G: {}; ".format(number_of_gyms)
    if args.pokestops:
        name += "P: {}".format(number_of_pokestops)
        if args.gyms:
            name += " (M: {})".format(next_gym_short)
        name += "; "
    name += "{}</name>".format( escape(cellID.ToToken()) )
    
    #construct description
    if args.descriptions:
        description = "<description>"
        if args.gyms:
            description += ("Gyms ({}):&lt;br/&gt;".format(number_of_gyms) +
                            "&lt;br/&gt;" +
                            "&lt;br/&gt;".join([  escape(n) for n in sorted([ g[3] for g in gyms_within_cell ])  ]))
            if args.pokestops:
                if gyms_within_cell:
                    description += "&lt;br/&gt;"
                description += ("&lt;br/&gt;" +
                                "&lt;br/&gt;")
        if args.pokestops:
            description += "PokéStops ({}".format(number_of_pokestops)
            if args.gyms:
                description += "; {}".format(next_gym_long)
            description += ("):&lt;br/&gt;" +
                            "&lt;br/&gt;" +
                            "&lt;br/&gt;".join([  escape(n) for n in sorted([ p[1] for p in pokestops_within_cell ])  ]))
        description += "</description>"
    else:
        description = ""
    
    #determine how cell needs to be stylised
    if number_of_gyms >= 0  and  number_of_gyms <= 3:
        styleURL = style_configs.get(str(number_of_gyms), DEFAULT_STYLEURL)
    else:
        styleURL = style_configs.get("*", DEFAULT_STYLEURL)
    
    print("<Placemark>" +
           name +
           description +
           "<styleUrl>{}</styleUrl>".format(escape(styleURL)) +
           "<Polygon>" +
            "<tessellate>1</tessellate>" +
            "<outerBoundaryIs>" +
             "<LinearRing>" +
              "<coordinates>{} {} {} {} {}</coordinates>".format( coordinates[0], coordinates[1], coordinates[2], coordinates[3], coordinates[0] ) +
             "</LinearRing>" +
            "</outerBoundaryIs>" +
           "</Polygon>" +
          "</Placemark>")


#optionally, generate TSV records of Gyms, each in addition with the count of Gyms within its own cell
if args.output_gyms_with_count_within_cell:
    if not args.gyms:
        warnings.warn("“--gyms FILE” not specified, thus “--output-gyms-with-count-within-cell FILE” will produce an empty file.")
    
    tsv_file = open(args.output_gyms_with_count_within_cell, "x")
    for g in gyms:
        tsv_file.write( "{}\t{}\t{}\t{}\n".format(g[1], g[2], g[3], g[4]) )
    tsv_file.close()


#analyse cells
if args.analyse:
    #print cell ID hex-tokens of all cells
    for c  in  sorted([ c.ToToken() for c in all_cellIDs ]):
        print("Gym Cell: {}".format(c) , file=sys.stderr)
    
    #print outmost maximum/minimum latitude and longitude of all cells
    all_vertices = [  S2LatLng( S2Cell(c).GetVertex(i) )  for c in all_cellIDs for i in range(4)  ]
    outmost_max_latitude = max([v.lat().degrees() for v in all_vertices])
    outmost_min_latitude = min([v.lat().degrees() for v in all_vertices])
    outmost_max_longitude = max([v.lng().degrees() for v in all_vertices])
    outmost_min_longitude = min([v.lng().degrees() for v in all_vertices])
    
    print("Outmost Maximum Latitude:  {}".format(outmost_max_latitude) , file=sys.stderr)
    print("Outmost Minimum Latitude:  {}".format(outmost_min_latitude) , file=sys.stderr)
    print("Outmost Maximum Longitude: {}".format(outmost_max_longitude) , file=sys.stderr)
    print("Outmost Minimum Longitude: {}".format(outmost_min_longitude) , file=sys.stderr)
















#Copyright © 2020, Christoph Anton Mitterer <mail@christoph.anton.mitterer.name>.
#All rights reserved.
#
#
#This program is free software: you can redistribute it and/or modify it under
#the terms of the GNU General Public License as published by the Free Software
#Foundation, either version 3 of the License, or (at your option) any later
#version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#PARTICULAR PURPOSE.
#See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License along with
#this program. If not, see <http://www.gnu.org/licenses/>.
