#!/usr/bin/python3
#coding=utf-8








import sys
from pywraps2 import S2CellId, S2CellUnion




#read cell ID hex-tokens
#Note: Since cellIDs is a list it may contain duplicates, moreover, its cells may be at any level and non-normalised. However, normalisation (which includes sorting out duplicates) takes place later, when normalised_cellIDs is obtained from the S2CellUnion created from the list.
cellIDs = [ S2CellId.FromToken( token.rstrip(), len(token.rstrip()) )  for  token in sys.stdin ]


#normalise cell IDs
normalised_cellIDs = S2CellUnion(cellIDs).cell_ids()


#print cell ID hex-tokens
for c  in  sorted([ c.ToToken() for c in normalised_cellIDs ]):
    print(c)
















#Copyright © 2020, Christoph Anton Mitterer <mail@christoph.anton.mitterer.name>.
#All rights reserved.
#
#
#This program is free software: you can redistribute it and/or modify it under
#the terms of the GNU General Public License as published by the Free Software
#Foundation, either version 3 of the License, or (at your option) any later
#version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#PARTICULAR PURPOSE.
#See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License along with
#this program. If not, see <http://www.gnu.org/licenses/>.
