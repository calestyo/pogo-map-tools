#!/usr/bin/python3
#coding=utf-8








import sys
import argparse
from xml.sax.saxutils import escape




#constants
DEFAULT_STYLEURL = "#point-default"




#parse command arguments
parser = argparse.ArgumentParser()
parser.add_argument("--style", nargs="?", default=None, const=DEFAULT_STYLEURL, type=str, help="records are stylised with STYLEURL", metavar="STYLEURL")
args = parser.parse_args()


#construct style
if args.style is not None:
    style = "<styleUrl>{}</styleUrl>".format(escape(args.style))
else:
    style = ""


#print KML records
for record in sys.stdin:
    record = record.rstrip().split("\t", 2)
    latitude = float(record[0])
    longitude = float(record[1])
    name = record[2]
    
    print( "<Placemark><name>{}</name>{}<Point><coordinates>{},{},0</coordinates></Point></Placemark>".format(escape(name), style, longitude, latitude) )
















#Copyright © 2020, Christoph Anton Mitterer <mail@christoph.anton.mitterer.name>.
#All rights reserved.
#
#
#This program is free software: you can redistribute it and/or modify it under
#the terms of the GNU General Public License as published by the Free Software
#Foundation, either version 3 of the License, or (at your option) any later
#version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#PARTICULAR PURPOSE.
#See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License along with
#this program. If not, see <http://www.gnu.org/licenses/>.
