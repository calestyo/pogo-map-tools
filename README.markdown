# PoGo Map Tools




## Overview

The purpose of these tools is to create [KML documents](https://ogc.org/standards/kml/) that visualise the location of Gyms and PokéStops as well as nominations or candidates within various levels of [S2 cells](https://s2geometry.io/).<br/>
This can for example help to determine those Gym cells (level 14) where further Gyms could get created or well-suited locations for PokéStop nominations in non-occupied PokéStop cells (level 17).

Input data, consisting of:
* [TSV](https://en.wikipedia.org/wiki/Tab-separated_values) files, containing the coordinates (latitude, longitude) of Gyms and PokéStops as well as nominations or candidates
* region specifiers, that determine one or more region(s) of the map to be considered
* KML snippets, that typically define the styles to be used

is transformed in several steps into:
* other TSV files
* KML records (which are not complete KML documents but just snippets of such)
* (final) complete KML documents

with each tool being responsible for more or less one specific task.


### `filter-tsv.py`
Filters `{latitude, longitude, name}`-records read from standard input and writes the results to standard output.

* The filtering is based on any (inclusive) disjunction of spherical rectangles (spanned by minimum/maximum latitudes and longitudes), simple polygons (specified via their vertices), cells (specified via their Cell ID Hex-Tokens), Gym cells covering spherical rectangles, simple polygons respectively cells or circles (spanned by radiuses (in metres) around centres).


### `tsv_to_kml-placemark-records.py`
Transforms `{latitude, longitude, name}`-records read from standard input to KML placemarks and writes them (one per line) to standard output.

* Optionally, a style can be applied to all placemarks.
* The generated output is not a complete/valid KML document, but only records of such.


### `s2-gym-cells_to_kml-records.py`
Covers or fills (that is: covering the interior) one or more region(s) (spherical rectangles (spanned by minimum/maximum latitudes and longitudes), simple polygons (specified via their vertices) or cells at any level (specified via their Cell ID Hex-Tokens)) with Gym cells (level 14), each being an independent polygon (and not as a grid), and writes them (one per line) to standard output.

* Each cell is named:<br/>
  `G: `*number of Gyms found within the cell*`; P: `*number of PokéStops found within the cell*` (M: `*expected number of missing PokéStops for the next Gym to appear*`); `*Cell ID Hex-Token*<br/>
  “⛔” is used to denote cells where expectedly no further Gyms are possible (the maximum is typically 3) and “❓” for those where the status is unknown or the next Gym would have already been expected.
* Per default, each cell gets a description containing additionally the lists of Gyms and PokéStops within it.
* Optionally, each cell is stylised based on the number ([0; 3]) of Gyms found within it. The wildcard symbol `*` allows to set a style for numbers > 3.<br/>
  This allows to quickly identify those cells, where further Gyms could be created.
* Gyms and PokéStops are each read from a TSV file (containing `{latitude, longitude, name}`-records).<br/>
  Since only these are used, they should typically **completely cover/fill the Gym cells resulting of the specified region(s)**; otherwise the calculations and stylisations might be obviously wrong. On the other hand, in order to avoid computationally intensive searches for contained Gyms and PokéStops, they **shouldn’t cover much more than that**.
* Notice that specifying (in sum) a large overall region to be covered/filled can easily lead to considerable computational effort, especially when the lists of Gyms and PokéStops are large, too.
* Optionally, a new TSV file for the Gyms is generated, containing `{latitude, longitude, name, count of Gyms found within its own cell}`-records (with the count being “0” if not contained in any of those generated).<br/>
  This allows to use different styles (typically icons) for the Gyms, based on the count of Gyms found within their respective cell.
* The generated output is not a complete/valid KML document, but only records of such.


### `s2-pokestop-cells_to_kml-records.py`
Covers or fills (that is: covering the interior) one or more region(s) (spherical rectangles (spanned by minimum/maximum latitudes and longitudes), simple polygons (specified via their vertices) or cells at any level (specified via their Cell ID Hex-Tokens)) with PokéStop cells (level 17), each being an independent polygon (and not as a grid), and writes them (one per line) to standard output.

* Each cell is named:<br/>
  *Cell ID Hex-Token*
* Optionally, each cell is stylised based on which (type of) occupants are (first) found within it.<br/>
  This allows to quickly identify those cells, which are already occupied by a Gym or PokéStop (there can be typically only one of them) as well as for example those for which nominations have already been submitted or candidates exist.<br/>
  Notice that only the first type of occupants found will be considered.
* Occupants of a type are each read from a TSV file (containing `{latitude, longitude, *}`-records).<br/>
  Since only these are used, they should typically **completely cover/fill the PokéStop cells resulting of the specified region(s)**; otherwise the stylisations might be obviously wrong. On the other hand, in order to avoid computationally intensive searches for contained occupants, they **shouldn’t cover much more than that**.
* Notice that specifying (in sum) a large overall region to be covered/filled can easily lead to considerable computational effort, especially when the lists of occupants are large, too.
* The generated output is not a complete/valid KML document, but only records of such.


### `s2-cells_kml-records.py`
Covers or fills (that is: covering the interior) one or more region(s) (spherical rectangles (spanned by minimum/maximum latitudes and longitudes), simple polygons (specified via their vertices) or cells at any level (specified via their Cell ID Hex-Tokens)) with cells at a specified level or uses cells (specified via their Cell ID Hex-Tokens) at any level, each being an independent polygon (and not as a grid), and writes them (one per line) to standard output.

* Each cell is named:<br/>
  *Cell ID Hex-Token*
* Per default, each cell gets a description containing the coordinates (latitude, longitude) of its vertices.
* Optionally, a style can be applied to all placemarks.
* The generated output is not a complete/valid KML document, but only records of such.


### `kml-records_to_kml.py`
Reads records of KML (one per line) from standard input and generates one or more complete KML document(s) from them.

* Output is written to standard output (only possible, when exactly one KML document is generated) or one or more file(s).
* Optionally, an extra header and/or footer can be inserted in the generated KML document(s).<br/>
  These will typically contain styling information.
* Optionally, output can be split into multiple KML documents after a given number of records.<br/>
  This helps to use large number of records with services that allow only a certain number of them per document. For example, Google My Maps has currently a limit of 2000.


### `normalise-cellIDs.py`
Reads Cell ID Hex-Tokens from standard input, normalises them and prints those to standard output.

Normalising means that (through all levels) any group of 4 (adjacent) cells is replaced by their parent cell, when possible.




## Requirements

[Python](https://python.org/) is required as well as the following modules, which are not part of the Python Standard Library:

Tool                                  | required                                           | optional
------------------------------------- | -------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
`filter-tsv.py`                       |                                                    | [s2geometry](https://github.com/google/s2geometry), for filtering of records within simple polygons or cells<br/>[geopy](https://github.com/geopy/geopy), for filtering of records within circles
`kml-records_to_kml.py`               |                                                    | 
`normalise-cellIDs.py`                | [s2geometry](https://github.com/google/s2geometry) | 
`s2-cells_kml-records.py`             | [s2geometry](https://github.com/google/s2geometry) | 
`s2-gym-cells_to_kml-records.py`      | [s2geometry](https://github.com/google/s2geometry) | 
`s2-pokestop-cells_to_kml-records.py` | [s2geometry](https://github.com/google/s2geometry) | 
`tsv_to_kml-placemark-records.py`     |                                                    | 




## Usage Examples
The following are typical examples of using these tools:

Input data:
* `all_gyms.tsv`, containing `{latitude, longitude, name}`-records of all (known) Gyms
* `all_pokestops.tsv`, containing `{latitude, longitude, name}`-records of all (known) PokéStops
* a region to be filled, here a spherical rectangle spanned by the northwest point `48.1450 11.5660` and the southeast point `48.1324 11.5834`

In these examples `filter-tsv.py`’s option `--within-gym-cells-covered-rectangle` is used, which assures that regardless of whether filling or covering a region, only one region is required for **both**, generating cells **as well as** filtering Gyms and PokéStops as well as nominations or candidates.<br/>
Note that when covering (in contrast to filling, where this doesn’t apply) regions and `filter-tsv.py` is used with anything but `--within-gym-cells-covered-rectangle`, `--within-gym-cells-covered-simple-polygon` respectively `--within-gym-cells-covered-cells`, it’s usually required to work with pairs of regions (see below at “The need for inner and outer regions when covering regions”).

*The retrieval of the original Gym- and PokéStop-records (as in `all_gyms.tsv` and `all_pokestops.tsv`) is beyond the scope of this document respectively the described tools.*

A [web version of these examples](https://www.google.com/maps/d/drive?state=%7B%22ids%22%3A%5B%221SvF8YRJPJkQXdP9HsMbeeia0z0I_IMAi%22%5D%2C%22action%22%3A%22open%22%2C%22userId%22%3A%22102329811815220905914%22%7D&usp=sharing) can be found on Google My Maps.


### 1. Filtering Gyms and PokéStops
```sh
$ filter-tsv.py --within-gym-cells-covered-rectangle 48.1450 11.5660  48.1324 11.5834 \
                < all_gyms.tsv > my_gyms.tsv
$ filter-tsv.py --within-gym-cells-covered-rectangle 48.1450 11.5660  48.1324 11.5834 \
                < all_pokestops.tsv > my_pokestops.tsv
```

*Of course, TSV files with any other type of placemarks (for example nominations or candidates) can be filtered, too.*


### 2. Generating Gym cells
Notice that it might be necessary to quote `*`, `#` and possibly more in the Shell Command Language.

a) With only a single default style for all cells:
```sh
$ s2-gym-cells_to_kml-records.py --cover-rectangle 48.1450 11.5660  48.1324 11.5834 \
                                 --gyms my_gyms.tsv --pokestops my_pokestops.tsv \
                                 > my_gym-cells.kml-records
```

b) With stylisations per cell, based on its number of contained Gyms:
```sh
$ s2-gym-cells_to_kml-records.py --cover-rectangle 48.1450 11.5660  48.1324 11.5834 \
                                 --gyms my_gyms.tsv --pokestops my_pokestops.tsv \
                                 --stylise-cells-per-number-of-gyms 0   '#s2cell_gym_0-gyms' \
                                 --stylise-cells-per-number-of-gyms 1   '#s2cell_gym_1-gyms' \
                                 --stylise-cells-per-number-of-gyms 2   '#s2cell_gym_2-gyms' \
                                 --stylise-cells-per-number-of-gyms 3   '#s2cell_gym_3-gyms' \
                                 --stylise-cells-per-number-of-gyms '*' '#s2cell_gym_*-gyms' \
                                 > my_gym-cells.kml-records
```

c) With stylisations per cell, based on its number of contained Gyms, as well as the generation of a new TSV file for the Gyms, with records additionally containing the count of Gyms found within their respective cell or “0” if not contained in any of those generated:
```sh
$ s2-gym-cells_to_kml-records.py --cover-rectangle 48.1450 11.5660  48.1324 11.5834 \
                                 --gyms my_gyms.tsv --pokestops my_pokestops.tsv \
                                 --stylise-cells-per-number-of-gyms 0   '#s2cell_gym_0-gyms' \
                                 --stylise-cells-per-number-of-gyms 1   '#s2cell_gym_1-gyms' \
                                 --stylise-cells-per-number-of-gyms 2   '#s2cell_gym_2-gyms' \
                                 --stylise-cells-per-number-of-gyms 3   '#s2cell_gym_3-gyms' \
                                 --stylise-cells-per-number-of-gyms '*' '#s2cell_gym_*-gyms' \
                                 --output-gyms-with-count-within-cell my_gyms-with-count-within-cell.tsv \
                                 > my_gym-cells.kml-records
```
*See below at (4.c) for how `my_gyms-with-count-within-cell.tsv` can be used.*


### 3. Generating PokéStop cells
Notice that it might be necessary to quote `#` and possibly more in the Shell Command Language.

a) With only a single default style for all cells:
```sh
$ s2-pokestop-cells_to_kml-records.py --cover-rectangle 48.1450 11.5660  48.1324 11.5834 \
                                      > my_pokestop-cells.kml-records
```

b) With stylisations per cell, based on which type of occupants is (first) found within it:
```sh
$ s2-pokestop-cells_to_kml-records.py --cover-rectangle 48.1450 11.5660  48.1324 11.5834 \
                                      --stylise-occupied-cells my_gyms.tsv        '#s2cell_pokestop_occupiedWithGym' \
                                      --stylise-occupied-cells my_pokestops.tsv   '#s2cell_pokestop_occupiedWithPokestop' \
                                      > my_pokestop-cells.kml-records
```
This can be extended with any other type of occupants, for example:
```sh
                                      --stylise-occupied-cells my_nominations.tsv '#s2cell_pokestop_containsNomination' \
                                      --stylise-occupied-cells my_candidates.tsv  '#s2cell_pokestop_containsCandidate' \
```
where nominations are those which have already been submitted (but not yet accepted or rejected) to Wayfarer and candidates are merely ideas for possible nominations.


### 4. Generating placemarks (such as for Gyms and PokéStops as well as nominations or candidates)
Notice that it might be necessary to quote `#` and possibly more in the Shell Command Language.

a) With a default style:
```sh
$ tsv_to_kml-placemark-records.py < my_pokestops.tsv > my_pokestops.kml-records
```

b) With different stylisations per type of placemarks:
```sh
$ tsv_to_kml-placemark-records.py --style '#point-gym'        < my_gyms.tsv        > my_gyms.kml-records
$ tsv_to_kml-placemark-records.py --style '#point-pokestop'   < my_pokestops.tsv   > my_pokestops.kml-records
$ tsv_to_kml-placemark-records.py --style '#point-nomination' < my_nominations.tsv > my_nominations.kml-records
$ tsv_to_kml-placemark-records.py --style '#point-candidate'  < my_candidates.tsv  > my_candidates.kml-records
```

c) Stylising Gyms, based on the count of Gyms found within their respective cell:<br/>
Notice that it might be necessary to quote `#` and possibly more in the Shell Command Language.<br/>
*See above at (2.c) for how `my_gyms-with-count-within-cell.tsv` can be generated.*
```sh
$ { awk -F '\t' '$4==0 {OFS="\t"; print $1, $2, $3}' my_gyms-with-count-within-cell.tsv  |  tsv_to_kml-placemark-records.py --style '#point-gym' ;
    awk -F '\t' '$4==1 {OFS="\t"; print $1, $2, $3}' my_gyms-with-count-within-cell.tsv  |  tsv_to_kml-placemark-records.py --style '#point-gym-blue-1' ;
    awk -F '\t' '$4==2 {OFS="\t"; print $1, $2, $3}' my_gyms-with-count-within-cell.tsv  |  tsv_to_kml-placemark-records.py --style '#point-gym-yellow-2' ;
    awk -F '\t' '$4==3 {OFS="\t"; print $1, $2, $3}' my_gyms-with-count-within-cell.tsv  |  tsv_to_kml-placemark-records.py --style '#point-gym-red-3' ;
    awk -F '\t' '$4>=4 {OFS="\t"; print $1, $2, $3}' my_gyms-with-count-within-cell.tsv  |  tsv_to_kml-placemark-records.py --style '#point-gym-purple-+' ;
  } > my_gyms-with-count-within-cell.kml-records
```
The pattern `$4==n` selects those Gyms from `my_gyms-with-count-within-cell.tsv` which are not contained in any of the generated cells (for n = 0) respectively whose count of Gyms found within their respective cell is n (for n > 0). `$4>=n` selects those Gyms whose count of Gyms found within their respective cell is ≧ n (for n > 0).


### 5. Generating complete KML documents from KML records
a) Writing a single KML document with unsorted records and no styles to standard output:
```sh
$ kml-records_to_kml.py < my_gyms.kml-records > my_gyms.kml
```

b) Writing a single KML document with sorted records and no styles to standard output:<br/>
All the tools generate records, where the first variable part is the respective name. Thus records can be easily sorted, like this:
```sh
$ sort -V my_gyms.kml-records  |  kml-records_to_kml.py > my_gyms.kml
```

c) Writing a single KML document with sorted records and styles (taken from a KML snippet file) to a file:
```sh
$ sort -V my_gyms.kml-records  |  kml-records_to_kml.py --extra-header styles.kml-snippet --output my_gyms.kml
```

d) Splitting output into multiple KML files, each with at most 20 records, with sorted records and styles (taken from a KML snippet file):
```sh
$ sort -V my_gyms.kml-records  |  kml-records_to_kml.py --extra-header styles.kml-snippet --split-after 20 --output my_gyms.kml
```
The files will be named `my_gyms.0.kml`, `my_gyms.1.kml`,… if a split is actually necessary.

For example, to create KML documents for all the KML record files created in the examples above one would do something like this:<br/>
*Either:*
```sh
$ sort -V my_gyms.kml-records                         |  kml-records_to_kml.py --extra-header styles.kml-snippet --split-after 2000 --output my_gyms.kml
```
*or:*
```sh
$ sort -V my_gyms-with-count-within-cell.kml-records  |  kml-records_to_kml.py --extra-header styles.kml-snippet --split-after 2000 --output my_gyms-with-count-within-cell.kml
```
*as well as:*
```sh
$ sort -V my_pokestops.kml-records                    |  kml-records_to_kml.py --extra-header styles.kml-snippet --split-after 2000 --output my_pokestops.kml
$ sort -V my_nominations.kml-records                  |  kml-records_to_kml.py --extra-header styles.kml-snippet --split-after 2000 --output my_nominations.kml
$ sort -V my_candidates.kml-records                   |  kml-records_to_kml.py --extra-header styles.kml-snippet --split-after 2000 --output my_candidates.kml
$ sort -V my_gym-cells.kml-records                    |  kml-records_to_kml.py --extra-header styles.kml-snippet --split-after 2000 --output my_gym-cells.kml
$ sort -V my_pokestop-cells.kml-records               |  kml-records_to_kml.py --extra-header styles.kml-snippet --split-after 2000 --output my_pokestop-cells.kml
```


### 6. Splitting up KML record files, based on the style of records:
It might be useful to split up KML record files, based on the respective style of the records, in order to get different KML documents respectively map layers for different types of objects.

One could for example split into the following types of objects:
* all Gym cells, containing either of:
  * 0 Gyms,
  * 1 Gyms,
  * 2 Gyms,
  * 3 Gyms,
  * or > 3 Gyms
* all PokéStop cells, being occupied with respectively containing either of:
  * Gyms,
  * PokéStops,
  * Nominations,
  * or Candidates
* all Gyms, whose respective Gym cell contains either of:
  * 1 Gyms,
  * 2 Gyms,
  * 3 Gyms,
  * or > 3 Gyms

The splitting could be done for each type of objects (respectively style) like this:
```sh
$ grep -F '<styleUrl>#s2cell_pokestop_occupiedWithGym</styleUrl>'      my_pokestop-cells.kml-records > my_pokestop-cells-occupied-with-gym.kml-records
$ grep -F '<styleUrl>#s2cell_pokestop_occupiedWithPokestop</styleUrl>' my_pokestop-cells.kml-records > my_pokestop-cells-occupied-with-pokestop.kml-records
```


### 7. Using simple polygons or cells as region(s)
In addition to spherical rectangles, simple polygons or cells at any level ([0; 30]) can be used for specifying regions. This allows for more granular regions, especially when the desired regions don’t have rectangular shapes.<br/>
At the applicable tools, simple polygons are specified via ordered vertices (shaping single non-intersecting closed paths, which, together with their respectively enclosed area, form regions) and cells via Cell ID Hex-Tokens.

In order to determine the Cell ID Hex-Tokens for a desired region one may use `s2-cells_kml-records.py` to cover/fill a larger area with cells and then select only those that fit.<br/>
This could be done even several times with cells at different levels, for example using lower level (that is: larger) cells for the bulk parts and higher level (that is: smaller) cells for details of the desired region.

The actual selection process can be done in any graphical KML editor (for example Google My Maps) by removing the undesired cells. The remaining ones are then stored in a KML document and their Cell ID Hex-Tokens are extracted from that, for example using `libxml2`’s `xmllint`:
```sh
$ xmllint --xpath '//*[local-name()="Placemark"]/*[local-name()="name"]/text()' desired-cells.kml > cell_ID_hex-tokens
```

Cell ID Hex-Tokens for covering/filling mostly the same region as used in these examples would be (for example) the level 14 cells `479ddf59 479ddf5f 479ddf61 479ddf63 479e7589 479e758b 479e758d 479e758f 479e7593 479e75ed 479e75ef 479e75f1 479e75f3 479e75f5 479e75f7`.




## Miscellaneous


### Determining the exact coordinates of nominations within Wayfarer
1. Select the desired entry at [Wayfarer Nominations](https://wayfarer.nianticlabs.com/nominations).
2. At one corner of the location map there is a link to the exact coordinates in the form of the Google-logo.


### Covering vs. filling regions
Covering a region means, that as many cells are generated so that at least the whole region is covered by them.<br/>
Since cells at a given level have a fixed size and position, this means that most likely they cover more than the actually specified region.<br/>
A limit for the maximum number of cells per region (`--max-records-per-region`) will be ignored.

Filling a region means, that as much as possible of the region’s interior (which includes its own border) is covered by cells, without any of them overlapping outside of the region.<br/>
Since cells at a given level have a fixed size and position, this means that most likely they can’t cover all of the actually specified region.<br/>
A limit for the maximum number of cells per region (`--max-records-per-region`) will be adhered.

Also note that vertices and edges are part of their respective cells (and their interiors), which also means that any cell covers, fills and contains itself.


### The need for inner and outer regions when covering regions
**When covering (in contrast to filling, where all this doesn’t apply)** regions, one might need to use pairs of regions with the tools, each consisting of:
* an inner region, which is used for generating cells (with `s2-gym-cells_to_kml-records.py` and `s2-pokestop-cells_to_kml-records.py`)
* an outer region, which is used for filtering Gyms and PokéStops as well as nominations or candidates (with `filter-tsv.py`)

The reason is:<br/>
For the stylisations (like which PokéStop cells are occupied) and calculations (like how many and which Gyms/PokéStops are contained within a Gym cell or when the next Gym expectedly appears) to work properly, the tools must know about all Gyms and PokéStops as well as nominations or candidates within the generated cells.<br/>
The cells (which have a fixed size and position) generated when covering a region, most likely cover more than the actually specified region.<br/>
Therefore it’s not enough to use the same region to also filter Gyms and PokéStops as well as nominations or candidates (which are all points), as some, which are however contained within the excess parts of the outer generated cells, could be lost.

An exception to this is:<br/>
When only using (the same) cells with a level of Gym cells or lower (that is [0; 14]) as regions for **both**, generating cells (with `s2-gym-cells_to_kml-records.py` and `s2-pokestop-cells_to_kml-records.py`), **as well as** filtering Gyms and PokéStops as well as nominations or candidates (with `filter-tsv.py`), there is obviously no need for outer regions, even when covering.<br/>
However, when cells of a higher level (that is > 14) are used and these are not contained within other (used) cells that fulfil the above requirement, this exception doesn’t hold true anymore.

One way to determine a suitable pair of inner and outer region is as follows:
1. One starts with the inner region, generates the (Gym and/or PokéStop) cells and adjusts the region until the desired area is covered.
2. The `--analyse` option of `s2-gym-cells_to_kml-records.py` and `s2-pokestop-cells_to_kml-records.py` prints the outmost maximum/minimum latitude and longitude of all cells.
3. These values (or maybe some slightly larger ones) can then be used for a spherical rectangle as the outer region.

**Alternatively one should however just follow either of these recommendations:**
* Only use filling (and not covering), where all this doesn’t apply.
* Use `filter-tsv.py`’s options `--within-gym-cells-covered-rectangle`, `--within-gym-cells-covered-simple-polygon` respectively `--within-gym-cells-covered-cells` with the same region(s) as when generating cells (with `s2-gym-cells_to_kml-records.py` and `s2-pokestop-cells_to_kml-records.py`). These will cover the respective region(s) with Gym cells and filter based on those, which in turn assures that the generated cells are completely covered/filled with Gyms and PokéStops as well as nominations or candidates.
* Only use (the same) cells with a level of Gym cells or lower (that is [0; 14]) as regions for **both**, generating cells (with `s2-gym-cells_to_kml-records.py` and `s2-pokestop-cells_to_kml-records.py`), **as well as** filtering Gyms and PokéStops as well as nominations or candidates (with `filter-tsv.py`).




## Caveats
* Some of the tools assume, that each Gym, PokéStop or other occupant is contained within exactly one cell at a given level. It is unknown to the author whether they can be located exactly on cell edges or vertices.
* The rules and algorithms for Gyms, PokéStop and cells may obviously be changed at any time by the developers of Pokémon GO.
