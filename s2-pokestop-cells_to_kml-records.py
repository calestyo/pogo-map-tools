#!/usr/bin/python3
#coding=utf-8








import warnings
import sys
import argparse
from xml.sax.saxutils import escape
from pywraps2 import S2Cell, S2CellId, S2CellUnion, S2LatLng, S2LatLngRect, S2Loop, S2RegionCoverer




#constants
POKESTOP_S2_CELL_LEVEL = 17
DEFAULT_MAX_RECORDS_PER_REGION = 10000
DEFAULT_STYLEURL = "#s2cell_pokestop_default"




def latitude_degree(l):
    try:
        l = float(l)
    except ValueError:
        raise argparse.ArgumentTypeError("“{}” is not a floating-point number.".format(l))
    
    if l < -90  or  l > 90:
        raise argparse.ArgumentTypeError("“{}” is not within the range [-90.0; 90.0].".format(l))
    
    return l




def longitude_degree(l):
    try:
        l = float(l)
    except ValueError:
        raise argparse.ArgumentTypeError("“{}” is not a floating-point number.".format(l))
    
    if l < -180  or  l > 180:
        raise argparse.ArgumentTypeError("“{}” is not within the range [-180.0; 180.0].".format(l))
    
    return l




def point_degree(p):
    return ( latitude_degree(p[0]), longitude_degree(p[1]) )




def warn_max_records_per_region(l):
    if l == args.max_records_per_region:
        warnings.warn("Maximum number of records (cells) per region ({}) has been reached.".format(args.max_records_per_region))
    elif l > args.max_records_per_region:
        warnings.warn("Maximum number of records (cells) per region ({}) has been exceeded by {}).".format(args.max_records_per_region, l-args.max_records_per_region))




#parse command arguments
parser = argparse.ArgumentParser()
parser.add_argument("--cover-rectangle", nargs=4, default=[], action="append", type=float, help="cover the spherical rectangle spanned by LATITUDE-MAX,LONGITUDE-MIN and LATITUDE-MIN,LONGITUDE-MAX with cells", metavar=("LATITUDE-MAX", "LONGITUDE-MIN", "LATITUDE-MIN", "LONGITUDE-MAX"))
parser.add_argument("--fill-rectangle", nargs=4, default=[], action="append", type=float, help="fill (that is: cover the interior of) the spherical rectangle spanned by LATITUDE-MAX,LONGITUDE-MIN and LATITUDE-MIN,LONGITUDE-MAX with cells", metavar=("LATITUDE-MAX", "LONGITUDE-MIN", "LATITUDE-MIN", "LONGITUDE-MAX"))
parser.add_argument("--cover-simple-polygon", default=[], action="append", type=str, help="cover the simple polygon whose vertices are read from FILE with cells", metavar="FILE")
parser.add_argument("--fill-simple-polygon", default=[], action="append", type=str, help="fill (that is: cover the interior of) the simple polygon whose vertices are read from FILE with cells", metavar="FILE")
parser.add_argument("--cover-cells", default=[], action="append", type=str, help="cover the cells (at any level) whose Cell ID Hex-Tokens are read from FILE with cells", metavar="FILE")
parser.add_argument("--fill-cells", default=[], action="append", type=str, help="fill (that is: cover the interior of) the cells (at any level) whose Cell ID Hex-Tokens are read from FILE with cells", metavar="FILE")
parser.add_argument("--max-records-per-region", default=DEFAULT_MAX_RECORDS_PER_REGION, type=int, help="maximum number of records (cells) to be generated per region (ignored when covering)", metavar="NUMBER")
parser.add_argument("--stylise-occupied-cells", nargs=2, action="append", type=str, dest="style_configs", help="cells that contain occupants (for example Gyms or PokéStops) taken from FILE are stylised with STYLEURL", metavar=("FILE", "STYLEURL"))
parser.add_argument("--analyse", action="store_true", help="print additional information to standard error")
args = parser.parse_args()

#check whether valid latitudes and longitudes have been specified
for rectangle  in  args.cover_rectangle + args.fill_rectangle:
    latitude_degree(rectangle[0])
    longitude_degree(rectangle[1])
    latitude_degree(rectangle[2])
    longitude_degree(rectangle[3])


#build lists with cells for the records of each occupants TSV file
occupant_cells_per_style = []
if args.style_configs:
    for sc in args.style_configs:
        tsv_file = open(sc[0], "r")
        
        occupant_cells = []
        for record in tsv_file:
            record = record.rstrip().split("\t", 2)
            latitude = float(record[0])
            longitude = float(record[1])
            
            occupant_cells.append( S2Cell(S2LatLng.FromDegrees(latitude, longitude)) )
        
        occupant_cells_per_style.append(occupant_cells)
        tsv_file.close()


#cover/fill desired region(s) with POKESTOP_S2_CELL_LEVEL-level cells
#Note: Since all_cellIDs is a set it cannot contain duplicates, moreover, its cells are all at the same level. Therefore, all_cellIDs is automatically normalised at the fixed level.
all_cellIDs = set()
rc = S2RegionCoverer()
rc.set_fixed_level(POKESTOP_S2_CELL_LEVEL)
rc.set_max_cells(args.max_records_per_region)

for rectangle in args.cover_rectangle:
    northwest_vertex = S2LatLng.FromDegrees(rectangle[0], rectangle[1])
    southeast_vertex = S2LatLng.FromDegrees(rectangle[2], rectangle[3])
    region_cellIDs = rc.GetCovering( S2LatLngRect.FromPointPair(northwest_vertex, southeast_vertex) )
    warn_max_records_per_region(len(region_cellIDs))
    
    all_cellIDs.update(region_cellIDs)

for rectangle in args.fill_rectangle:
    northwest_vertex = S2LatLng.FromDegrees(rectangle[0], rectangle[1])
    southeast_vertex = S2LatLng.FromDegrees(rectangle[2], rectangle[3])
    region_cellIDs = rc.GetInteriorCovering( S2LatLngRect.FromPointPair(northwest_vertex, southeast_vertex) )
    warn_max_records_per_region(len(region_cellIDs))
    
    all_cellIDs.update(region_cellIDs)

for simple_polygon_file_name in args.cover_simple_polygon:
    simple_polygon_file = open(simple_polygon_file_name, "r")
    simple_polygon_file_vertices = [ S2LatLng.FromDegrees(  *point_degree( vertex.rstrip().split(" ", 1) )  ).ToPoint()  for  vertex in simple_polygon_file ]
    simple_polygon_file.close()
    
    simple_polygon = S2Loop(simple_polygon_file_vertices)
    #normalise the loop (if necessary), which is required because it’s vertices could have been given in clockwise orientation (which would typically lead to an extremely large simple polygon)
    simple_polygon.Normalize()
    
    region_cellIDs = rc.GetCovering(simple_polygon)
    warn_max_records_per_region(len(region_cellIDs))
    
    all_cellIDs.update(region_cellIDs)

for simple_polygon_file_name in args.fill_simple_polygon:
    simple_polygon_file = open(simple_polygon_file_name, "r")
    simple_polygon_file_vertices = [ S2LatLng.FromDegrees(  *point_degree( vertex.rstrip().split(" ", 1) )  ).ToPoint()  for  vertex in simple_polygon_file ]
    simple_polygon_file.close()
    
    simple_polygon = S2Loop(simple_polygon_file_vertices)
    #normalise the loop (if necessary), which is required because it’s vertices could have been given in clockwise orientation (which would typically lead to an extremely large simple polygon)
    simple_polygon.Normalize()
    
    region_cellIDs = rc.GetInteriorCovering(simple_polygon)
    warn_max_records_per_region(len(region_cellIDs))
    
    all_cellIDs.update(region_cellIDs)

for cells_file_name in args.cover_cells:
    cells_file = open(cells_file_name, "r")
    cells_file_cellIDs = [ S2CellId.FromToken( token.rstrip(), len(token.rstrip()) )  for  token in cells_file ]
    cells_file.close()
    region_cellIDs = rc.GetCovering( S2CellUnion(cells_file_cellIDs) )
    warn_max_records_per_region(len(region_cellIDs))
    
    all_cellIDs.update(region_cellIDs)

for cells_file_name in args.fill_cells:
    cells_file = open(cells_file_name, "r")
    cells_file_cellIDs = [ S2CellId.FromToken( token.rstrip(), len(token.rstrip()) )  for  token in cells_file ]
    cells_file.close()
    region_cellIDs = rc.GetInteriorCovering( S2CellUnion(cells_file_cellIDs) )
    warn_max_records_per_region(len(region_cellIDs))
    
    all_cellIDs.update(region_cellIDs)


#print KML records
for cellID in all_cellIDs:
    cell = S2Cell(cellID)
    vertices = [ S2LatLng(cell.GetVertex(i))  for i in range(4) ]
    coordinates = [ "{},{},0".format( v.lng().degrees(), v.lat().degrees() )  for v in vertices ]
    
    #determine whether cell needs to be stylised
    styleURL = DEFAULT_STYLEURL
    occupant_found = False
    for i in range(len(occupant_cells_per_style)):
        for c in occupant_cells_per_style[i]:
            if cell.Contains(c):
                occupant_found = True
                styleURL = args.style_configs[i][1]
                break
        #use the style from the first matching style configuration
        if occupant_found:
            break
    
    print("<Placemark>" +
           "<name>{}</name>".format( escape(cellID.ToToken()) ) +
           "<styleUrl>{}</styleUrl>".format(escape(styleURL)) +
           "<Polygon>" +
            "<tessellate>1</tessellate>" +
            "<outerBoundaryIs>" +
             "<LinearRing>" +
              "<coordinates>{} {} {} {} {}</coordinates>".format( coordinates[0], coordinates[1], coordinates[2], coordinates[3], coordinates[0] ) +
             "</LinearRing>" +
            "</outerBoundaryIs>" +
           "</Polygon>" +
          "</Placemark>")


#analyse cells
if args.analyse:
    #print cell ID hex-tokens of all cells
    for c  in  sorted([ c.ToToken() for c in all_cellIDs ]):
        print("PokéStop Cell: {}".format(c) , file=sys.stderr)
    
    #print outmost maximum/minimum latitude and longitude of all cells
    all_vertices = [  S2LatLng( S2Cell(c).GetVertex(i) )  for c in all_cellIDs for i in range(4)  ]
    outmost_max_latitude = max([v.lat().degrees() for v in all_vertices])
    outmost_min_latitude = min([v.lat().degrees() for v in all_vertices])
    outmost_max_longitude = max([v.lng().degrees() for v in all_vertices])
    outmost_min_longitude = min([v.lng().degrees() for v in all_vertices])
    
    print("Outmost Maximum Latitude:  {}".format(outmost_max_latitude) , file=sys.stderr)
    print("Outmost Minimum Latitude:  {}".format(outmost_min_latitude) , file=sys.stderr)
    print("Outmost Maximum Longitude: {}".format(outmost_max_longitude) , file=sys.stderr)
    print("Outmost Minimum Longitude: {}".format(outmost_min_longitude) , file=sys.stderr)
















#Copyright © 2020, Christoph Anton Mitterer <mail@christoph.anton.mitterer.name>.
#All rights reserved.
#
#
#This program is free software: you can redistribute it and/or modify it under
#the terms of the GNU General Public License as published by the Free Software
#Foundation, either version 3 of the License, or (at your option) any later
#version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#PARTICULAR PURPOSE.
#See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License along with
#this program. If not, see <http://www.gnu.org/licenses/>.
